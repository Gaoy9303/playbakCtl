#include "timepainter.h"

TimePainter::TimePainter(QWidget *parent) : QMainWindow(parent)
{

}


void TimePainter::TimePaint_setPara(int width,int interval){
    this->m_width = width;
    this->m_timeInterval = interval;
}


void TimePainter::paintEvent(QPaintEvent *Event ){
   Q_UNUSED(Event);
   QPainter painter(this);
   painter.setPen(QColor(255,255,255));
   painter.setRenderHint(QPainter::Antialiasing, true);

   int h,m;
   int txt_x;
   char tims[16]={'\0'};
   for(int i=0;;i++){
       h=(i*m_timeInterval)/3600;
       m=(i*m_timeInterval%3600)/60;
       txt_x = m_width*i;
       if((long)i*m_timeInterval <= m_secTotal+m_timeInterval){
           painter.drawLine(QPointF(0,DATA_ROW_HEIGHT), QPointF(0,DATA_ROW_HEIGHT));
           sprintf(tims,"%02d:%02d",h,m);
           if(QString(tims).contains("24:")){
               painter.drawText(QPoint(txt_x-15,DATA_ROW_HEIGHT), QString("24"));
           }else{
               painter.drawText(QPoint(txt_x,DATA_ROW_HEIGHT), QString(tims));
           }
       }else{
           break;
       }
   }
}
