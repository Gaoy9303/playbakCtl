#ifndef PLAYBAKCTL_WIDGET_H
#define PLAYBAKCTL_WIDGET_H

#include <QWidget>
#include <QDesktopWidget>
#include <QToolButton>
#include <QDebug>
#include <QLabel>
#include <QDesktopWidget>
#include <QBrush>
#include <QPoint>
#include <QCursor>
#include <QPainter>
#include <timepainter.h>
#include <QTimer>
#include <QDate>


#define DATA_ROW_HEIGHT (12)
#define EMPTY_ROW_HEIGHT (2)
#define CELL_COLUMN_WIDTH (1)
#define PRTLOG()  qDebug()<<__FILE__<<__LINE__


typedef enum _pbr_zoom{
    pbrZoom_24H=24/*1-bigCell 1h*/,
    pbrZoom_12H=24*2/*1-bigCell 20M*/,
    pbrZoom_4H=24*6/*1-bigCell 10M*/,
    pbrZoom_2H=24*12/*1-bigCell 5M*/
}pbrZoom;

namespace Ui {
class playbakCtl_Widget;
}

class playbakCtl_Widget : public QWidget
{
    Q_OBJECT

public:
    explicit playbakCtl_Widget(QWidget *parent = 0);
    ~playbakCtl_Widget();

    void playback_show();
private slots:
    void on_playback_process_tbW_cellEntered(int row, int column);

    void on_playbackCtl_Zoom24H_pBt_clicked();

    void on_playbackCtl_Zoom12H_pBt_clicked();

    void on_playbackCtl_Zoom4H_pBt_clicked();

    void on_playbackCtl_Zoom2H_pBt_clicked();

    void on_playback_process_tbW_doubleClicked(const QModelIndex &index);
    void ctrlTimertimeout_slot();

private:
    void playback_initProcessTbW();
    void paintEvent(QPaintEvent *Event );

    void playback_ZoomX_24H();
    void playback_ZoomX_12H();
    void playback_ZoomX_4H();
    void playback_ZoomX_2H();
    void  playback_stop();
    void  playback_Pause();
    void  playback_Play();
private:
    Ui::playbakCtl_Widget *ui;
    int           m_tbW_height_deault=20;
    int           m_tbW_width;
    int           m_tbW_height=0;

    int           m_bigCell_width;
    int           m_bigCell_num;
    int           m_bigCell_seconds;
    int           m_smlCellNum_InbigCell;
    float         m_smlCell_seconds;

    int           m_tbW_colNum;
    const int     m_tbW_row=1;
    int           m_tbW_rowNum;


    int           m_cu_col;
    int           m_cu_row;
    pbrZoom       m_pbrZoom=pbrZoom_12H;
    TimePainter*  m_timerPainter=NULL;


    int           m_selectedPlay_startCol=0;
    int           m_selectedPlay_Row=0;
    QTimer        m_ctrlTimer;
    QString       m_playbackStartTime;
    QDate         m_playbackDate;

    int           m_txthOffset=8;
    int           m_scrolBarH=20;

};

#endif // PLAYBAKCTL_WIDGET_H
