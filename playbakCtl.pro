#-------------------------------------------------
#
# Project created by QtCreator 2020-04-09T16:30:38
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = playbakCtl
TEMPLATE = app


SOURCES += main.cpp\
        playbakctl_widget.cpp \
    timepainter.cpp

HEADERS  += playbakctl_widget.h \
    timepainter.h

FORMS    += playbakctl_widget.ui
