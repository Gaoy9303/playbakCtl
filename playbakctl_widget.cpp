#include "playbakctl_widget.h"
#include "ui_playbakctl_widget.h"

#define desktop_width QApplication::desktop()->width()
#define desktop_height QApplication::desktop()->height()

playbakCtl_Widget::playbakCtl_Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::playbakCtl_Widget)
{
    ui->setupUi(this);
    int rows =2*m_tbW_row-1;
    m_tbW_rowNum =1+((rows<0)?0:rows);
    ui->playback_process_tbW->setMouseTracking(true);
    ui->playback_process_tbW->installEventFilter(this);
    this->setWindowFlags(Qt::FramelessWindowHint);
    this->setFixedWidth(desktop_width);
    ui->playback_process_tbW->setFixedHeight(60);
//    this->move(0,desktop_height-this->height());
    this->move(0,0);
    ui->process_frame->setAttribute(Qt::WA_TranslucentBackground,true);
    ui->playback_process_tbW->setAttribute(Qt::WA_TranslucentBackground,true);
    ui->playback_process_tbW->setSelectionMode(QAbstractItemView::NoSelection);
    ui->playback_process_tbW->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->playback_process_tbW->verticalHeader()->setSectionsClickable(false);
    ui->playback_process_tbW->horizontalHeader()->setVisible(false);
    ui->playback_process_tbW->horizontalHeader()->setStretchLastSection(false);
    ui->playback_process_tbW->verticalHeader()->setVisible(false); //隐藏列表头
    connect(&m_ctrlTimer,SIGNAL(timeout()),this,SLOT(ctrlTimertimeout_slot()));
    m_ctrlTimer.setInterval(1000);
    m_playbackStartTime = QString("00:00:00");
}

playbakCtl_Widget::~playbakCtl_Widget()
{
    delete ui;
}

void playbakCtl_Widget::playback_initProcessTbW(){
    m_bigCell_width = m_tbW_width/pbrZoom_24H;
    m_smlCellNum_InbigCell = (m_bigCell_width)/CELL_COLUMN_WIDTH;
    m_smlCell_seconds = (m_bigCell_seconds*1.0)/m_smlCellNum_InbigCell;
    m_tbW_colNum = (int)(m_smlCellNum_InbigCell*m_bigCell_num);

    ui->playback_process_tbW->setRowCount(m_tbW_rowNum);
    ui->playback_process_tbW->setColumnCount(m_tbW_colNum);
    for(int col=0;col<m_tbW_colNum;col++){
       ui->playback_process_tbW->setColumnWidth(col,CELL_COLUMN_WIDTH);
    }
    for(int row=0;row<m_tbW_rowNum;row++){
        if(row == 0){
            ui->playback_process_tbW->setRowHeight(row,DATA_ROW_HEIGHT);
            m_tbW_height+=DATA_ROW_HEIGHT;
        }else{
            if(row%2==1){
                ui->playback_process_tbW->setRowHeight(row,DATA_ROW_HEIGHT);
                m_tbW_height+=DATA_ROW_HEIGHT;
            }else{
                ui->playback_process_tbW->setRowHeight(row,EMPTY_ROW_HEIGHT);
                m_tbW_height+=EMPTY_ROW_HEIGHT;
            }
        }
    }
    m_tbW_height += m_tbW_height_deault;
    ui->playback_process_tbW->setFixedHeight(m_tbW_height);
    for(int col=0;col<m_tbW_colNum;col++){
       for(int row=0;row<m_tbW_rowNum;row++){
           QTableWidgetItem *item = new QTableWidgetItem();
           if(row==0){
               item->setBackground(QBrush(QColor(43,54,67)));
               item->setSelected(false);
           }else{
               if(row%2==1){
                    item->setBackground(Qt::green);
               }else{
                    item->setBackground(Qt::gray);
               }
               if(col%m_smlCellNum_InbigCell==0 && col != 0){
                   item->setBackground(Qt::gray);///
               }
           }
           ui->playback_process_tbW->setItem(row,col,item);
       }
    }
    ui->playback_process_tbW->setSpan(0,0,1,m_tbW_colNum);
    if(m_timerPainter != NULL){
        m_timerPainter->deleteLater();
        m_timerPainter = NULL;
    }else{
        m_timerPainter = new TimePainter(this);
        m_timerPainter->TimePaint_setPara(m_smlCellNum_InbigCell,m_bigCell_seconds);
        ui->playback_process_tbW->setCellWidget(0,0,m_timerPainter);//放置时间刻度轴
    }
}

void playbakCtl_Widget::on_playback_process_tbW_cellEntered(int row, int column)
{
    m_cu_col = column;
    m_cu_row = row;
    this->update();
}

void playbakCtl_Widget::paintEvent(QPaintEvent *){

    int col_x=QCursor::pos().x();
    int cuTime_x=10+ (ui->playback_process_tbW->pos().x())+col_x;
    int cuTime_y=ui->process_frame->y()+m_tbW_height+m_txthOffset+m_scrolBarH;

    QPainter painter(this);
    painter.setPen(Qt::white);
    painter.setRenderHint(QPainter::Antialiasing, true);
    if(cuTime_x >= m_tbW_width*9/10){
        cuTime_x -=50;
    }
    int sc=(m_cu_col*1.0)/(m_smlCellNum_InbigCell*m_bigCell_num)*24*3600;
    int ch=sc/3600;
    int cm = (sc%3600)/60;
    int cs = (sc%3600)%60;
    char ctims[16]={'\0'};sprintf(ctims,"%02d:%02d:%02d",ch,cm,cs);
    painter.drawText(QPoint(cuTime_x,cuTime_y), QString(ctims));///get the mouse's pos time
}


void playbakCtl_Widget::playback_ZoomX_24H(){
    m_bigCell_num=pbrZoom_24H;
    m_bigCell_seconds = 3600;
}
void playbakCtl_Widget::playback_ZoomX_12H(){
    m_bigCell_num=pbrZoom_12H;///Length x2   24x2
    m_bigCell_seconds = 1800;

}
void playbakCtl_Widget::playback_ZoomX_2H(){
    m_bigCell_num=pbrZoom_2H;
    m_bigCell_seconds = 300;



}
void playbakCtl_Widget::playback_ZoomX_4H(){
    m_bigCell_num=pbrZoom_4H;
    m_bigCell_seconds = 600;
}

void playbakCtl_Widget::playback_show(){
    this->show();
    m_tbW_height=0;
    if(m_timerPainter != NULL){
        m_timerPainter->deleteLater();
        m_timerPainter = NULL;
    }
    ui->playback_process_tbW->clear();
    m_tbW_width =ui->playback_process_tbW->width();
    switch(m_pbrZoom){
        case pbrZoom_24H:playback_ZoomX_24H();break;
        case pbrZoom_12H:playback_ZoomX_12H();break;
        case pbrZoom_4H:playback_ZoomX_4H();break;
        case pbrZoom_2H:playback_ZoomX_2H();break;
        default:playback_ZoomX_24H();break;
    }
    if(m_tbW_row <=2)
        m_txthOffset = 8+20;
    else
        m_txthOffset = -8;
    if(pbrZoom_24H==24)
        m_scrolBarH=0;
    else
        m_scrolBarH=20;
    this->playback_initProcessTbW();
}

void playbakCtl_Widget::on_playbackCtl_Zoom24H_pBt_clicked()
{
    m_pbrZoom = pbrZoom_24H;
    playback_show();
}

void playbakCtl_Widget::on_playbackCtl_Zoom12H_pBt_clicked()
{
    m_pbrZoom = pbrZoom_12H;
    playback_show();
}

void playbakCtl_Widget::on_playbackCtl_Zoom4H_pBt_clicked()
{

    m_pbrZoom = pbrZoom_4H;
    playback_show();
}

void playbakCtl_Widget::on_playbackCtl_Zoom2H_pBt_clicked()
{
    m_pbrZoom = pbrZoom_2H;
    playback_show();
}

void playbakCtl_Widget::on_playback_process_tbW_doubleClicked(const QModelIndex &index)
{
    if(index.row()%2){
        QTableWidgetItem *old_stem = ui->playback_process_tbW->item(m_selectedPlay_Row,m_selectedPlay_startCol);
        if(m_selectedPlay_startCol%m_smlCellNum_InbigCell==0 && m_selectedPlay_startCol != 0){
            old_stem->setBackground(Qt::gray);///
        }else{
            old_stem->setBackground(Qt::green);
        }
        if(m_playbackStartTime != QString("00:00:00")){
            playback_stop();
            m_playbackStartTime.clear();
        }
        m_selectedPlay_startCol = index.column();
        m_selectedPlay_Row = index.row();
        int sc=(m_selectedPlay_startCol*1.0)/(m_smlCellNum_InbigCell*m_bigCell_num)*24*3600;
        int ch=sc/3600;
        int cm = (sc%3600)/60;
        int cs = (sc%3600)%60;
        char ptime[16]={'\0'};sprintf(ptime,"%02d:%02d:%02d",ch,cm,cs);
        m_playbackStartTime = QString(ptime);
        QTableWidgetItem *new_stem = ui->playback_process_tbW->item(m_selectedPlay_Row,m_selectedPlay_startCol);
        new_stem->setBackground(Qt::red);
        playback_Play();
    }
}

void playbakCtl_Widget::ctrlTimertimeout_slot(){
    QTableWidgetItem *old_stem = ui->playback_process_tbW->item(m_selectedPlay_Row,m_selectedPlay_startCol);
    if(m_selectedPlay_startCol%m_smlCellNum_InbigCell==0 && m_selectedPlay_startCol != 0){
        old_stem->setBackground(Qt::gray);///
    }else{
        old_stem->setBackground(Qt::green);
    }
    m_selectedPlay_startCol++;
    QTableWidgetItem *new_stem = ui->playback_process_tbW->item(m_selectedPlay_Row,m_selectedPlay_startCol);
    new_stem->setBackground(Qt::red);
}

void  playbakCtl_Widget::playback_stop(){
    PRTLOG()<<"Stop playback"<<m_playbackStartTime;
    m_ctrlTimer.stop();
}
void  playbakCtl_Widget::playback_Pause(){
    PRTLOG()<<"Pause playback"<<m_playbackStartTime;
    m_ctrlTimer.stop();
}
void  playbakCtl_Widget::playback_Play(){
    PRTLOG()<<"Play playback"<<m_playbackStartTime;
    m_ctrlTimer.start();

}
