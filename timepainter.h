#ifndef TIMEPAINTER_H
#define TIMEPAINTER_H

#include <QMainWindow>
#include <QPaintEvent>
#include <QtMath>
#include <QDebug>
#include <QPainter>

#define DATA_ROW_HEIGHT (12)
class TimePainter : public QMainWindow
{
    Q_OBJECT
public:
    explicit TimePainter(QWidget *parent = 0);
    void TimePaint_setPara(int width,int interval);

    void paintEvent(QPaintEvent *Event );

signals:

public slots:
private:
    int         m_width;
    int         m_timeInterval;
    const long  m_secTotal = 24*3600;
};

#endif // TIMEPAINTER_H
